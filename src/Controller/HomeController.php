<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
     /**
     * @Route("/", name="neigh_home")
     */

    public function neig(){
        return $this->render('home/neig.html.twig');
    }

    
    /**
     * @Route("/first", name="un_home")
     */

    public function un(){
        return $this->render('home/un.html.twig');
    }
    /**
     * @Route("/second", name="deux_home")
     */

    public function deux(){
        return $this->render('home/deux.html.twig');
    }

   

    /**
     * @Route("/third", name="third_home")
     */

    public function trois(){
        return $this->render('home/trois.html.twig');
    }




}
